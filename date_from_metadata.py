import os
import numpy as np
import seaborn as sns
import pandas as pd
import datetime
from keras.preprocessing.image import load_img, img_to_array, array_to_img
from PIL import Image, ExifTags


__all__ = ['get_date', 'get_metadata']

def get_datetime_from_img(img):
    exif = { ExifTags.TAGS[k]: v for k, v in img._getexif().items() if k in ExifTags.TAGS }
    date_time_obj = datetime.datetime.strptime(exif['DateTime'], '%Y:%m:%d %H:%M:%S')
    
    return date_time_obj

def get_metadata_from_img(img):
    exif = { ExifTags.TAGS[k]: v for k, v in img._getexif().items() if k in ExifTags.TAGS }
    
    #ExifImageWidth = exif['ExifImageWidth']
    #ExifImageHeight = exif['ExifImageHeight']
    #ExposureMode = exif['ExifImageHeight']
    #Flash = exif['Flash']
    #WhiteBalance = exif['WhiteBalance']
    #SceneCaptureType = exif['SceneCaptureType']
    Make = exif['Make']
    Model = exif['Model']
    #ExposureTime = exif['ExposureTime']
    #XResolution = exif['XResolution']
    #YResolution = exif['YResolution']
    #ISOSpeedRatings = exif['ISOSpeedRatings']
    #ResolutionUnit = exif['ResolutionUnit']
    #ExifOffset = exif['ExifOffset']
    DateTimeOriginal = exif['DateTimeOriginal']
    
    return Make, Model
    
    

def get_date(df):
    """takes in a dataframe, 
       returns the dataframe with column containing the data in 
       the '%Y:%m:%d %H:%M:%S' format and a column containing a 
       sequence number, each sequence being for a unique value of date.
    """

    list_datetime = []
    list_images = df['Path']


    for k in list_images:
            img = Image.open(k)
            list_datetime.append(get_datetime_from_img(img))

    df['datetime'] = list_datetime
    df['date'] = df['datetime'].apply(lambda t: t.strftime('%Y-%M-%D %H:%M'))

    valeurs_dates = df['date'].unique()
    l = len(valeurs_dates)
    dictionnaire = dict(zip(valeurs_dates, range(l)))

    df['Passage'] = df['date'].map(dictionnaire)

    return df


def get_metadata(df):
    #list_datetime = []
    list_images = df['Path']
    list_make = []
    list_model = []

    for k in list_images:
            img = Image.open(k)
            #list_datetime.append(get_datetime_from_img(img))
            list_make.append(get_metadata_from_img(img)[0])
            list_model.append(get_metadata_from_img(img)[1])

       
    df['Marque'] = list_make
    df['Modele'] = list_model
    
    return df